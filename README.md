<!-- ![UpscaleBox logo](https://codeberg.org/DFCTeam/UpscaleBox/raw/branch/main/UpscaleBoxLogo.png) -->
<img src="https://codeberg.org/DFCTeam/UpscaleBox/raw/branch/main/UpscaleBoxLogo.png" height="300"></img>

# UpscaleBox
Simple discord bot to upscale images

# Setup & Run
Create ``inputs`` and ``outputs`` folders

Configure ``ub_config.py``

Run ``server.py`` first, then run ``discord_bot.py``