try:
    import ub_config
except ImportError:
    print("Error: ub_config.py is missing")
    exit()

import disnake as discord
from disnake.ext import commands

from PIL import Image

from httpx import AsyncClient, _exceptions

import random, string

import asyncio

activity = discord.Streaming(name=f"Upscaling images", url="https://www.twitch.tv/discord")
requests = AsyncClient(
    timeout = None
)

bot = commands.Bot(
    command_prefix=discord.ext.commands.when_mentioned,
    activity = activity,
	intents = discord.Intents.all(),
	allowed_mentions = discord.AllowedMentions.all()
)

taskRunningGen = 0

def gen_random_str(str_len):
    return ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=str_len))

@bot.event
async def on_ready():
    print(f"Working on {bot.user.name}#{bot.user.discriminator}")
    print()
    print("Guilds:")
    for guild in bot.guilds:
        print(f"- {guild.name} ({guild.id})")

@bot.event
async def on_guild_join(guild):
    print(f"Added on {guild.name} ({guild.id})")

@bot.event
async def on_guild_remove(guild):
    print(f"Removed from {guild.name} ({guild.id})")

@bot.slash_command(description = 'Upscale image')
async def upscale(
    ctx,
    image: discord.Attachment = commands.Param(description = "Your image"),
):
    mode = "Fast"

    user_id = ctx.author.id
    upscale_id = gen_random_str(10)

    await ctx.response.send_message(f"**{upscale_id}** - {ctx.author.mention} ({mode}) (Waiting to start)")

    global taskRunningGen

    while True:
        if taskRunningGen == 1:
            await asyncio.sleep(0.05)
        else:
            break

    await ctx.edit_original_response(f"**{upscale_id}** - {ctx.author.mention} (Starting...)")
    taskRunningGen = 1

    await image.save(f"./inputs/{upscale_id}.png")

    try:
        request = await requests.post(
            f"{ub_config.generate_server_url}/upscale",
            json = {
                "image_id": upscale_id,
            },
            timeout = 500
        )
        if request.status_code != 200:
            taskRunningGen = 0
            return await ctx.edit_original_response(f"**{upscale_id}** - {ctx.author.mention} (Failed - Something went wrong)")
    except _exceptions.ConnectTimeout:
        taskRunningGen = 0
        return await ctx.edit_original_response(f"**{upscale_id}** - {ctx.author.mention} (Failed - Server not available)")
    except _exceptions.ConnectError:
        taskRunningGen = 0
        return await ctx.edit_original_response(f"**{upscale_id}** - {ctx.author.mention} (Failed - Server not available)")

    taskRunningGen = 0

    output_filename = request.json()["path"]

    return await ctx.edit_original_response(
        f"**{upscale_id}** - {ctx.author.mention}",
        file = discord.File(f"./outputs/{upscale_id}.png"),
    )

bot.run(ub_config.token)