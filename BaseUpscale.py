import torch

from super_image import EdsrModel, MsrnModel
from super_image import PanModel
from super_image import ImageLoader

from PIL import Image

device = "cuda"

torch.backends.cudnn.benchmark = True
torch.backends.cuda.matmul.allow_tf32 = True

class UpscaleModelPan:
    def __init__(
        self,
        device: str = "cuda", # or cpu
        scale: int = 2,
        isBase: bool = True
    ) -> None:
        # eugenesiow/edsr
        # eugenesiow/edsr-base
        if isBase == True:
            self.model = PanModel.from_pretrained('eugenesiow/pan', scale=scale)
        else:
            self.model = PanModel.from_pretrained('eugenesiow/pan-bam', scale=scale)

        self.device = device

        self.model.to(self.device)
    
    def generate(
        self,
        image_id: str
    ):
        image = Image.open(f'./inputs/{image_id}.png')

        inputs = ImageLoader.load_image(image).to(self.device)
        preds = self.model(inputs)

        out_img_path = f'./outputs/{image_id}.png'

        ImageLoader.save_image(preds, out_img_path)

        return {"path": out_img_path}

class UpscaleModelEdsr:
    def __init__(
        self,
        device: str = "cuda", # or cpu
        scale: int = 2,
        isBase: bool = True
    ) -> None:
        # eugenesiow/edsr
        # eugenesiow/edsr-base
        if isBase == True:
            self.model = EdsrModel.from_pretrained('eugenesiow/edsr-base', scale=scale)
        else:
            self.model = EdsrModel.from_pretrained('eugenesiow/edsr', scale=scale)

        self.device = device

        self.model.to(self.device)
    
    def generate(
        self,
        image_id: str
    ):
        image = Image.open(f'./inputs/{image_id}.png')

        inputs = ImageLoader.load_image(image).to(self.device)
        preds = self.model(inputs)

        out_img_path = f'./outputs/{image_id}.png'

        ImageLoader.save_image(preds, out_img_path)

        return {"path": out_img_path}

class UpscaleModelMsrn:
    def __init__(
        self,
        device: str = "cuda", # or cpu
        scale: int = 2,
        isBase: bool = True
    ) -> None:
        # eugenesiow/msrn
        self.model = MsrnModel.from_pretrained('eugenesiow/msrn', scale=scale)

        self.device = device

        self.model.to(self.device)
    
    def generate(
        self,
        image_id: str
    ):
        image = Image.open(f'./inputs/{image_id}.png')

        inputs = ImageLoader.load_image(image).to(self.device)
        preds = self.model(inputs)

        out_img_path = f'./outputs/{image_id}.png'

        ImageLoader.save_image(preds, out_img_path)

        return {"path": out_img_path}