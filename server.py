# try:
#     import ub_config
# except ImportError:
#     print("Error: ub_config.py is missing")
#     exit()

import flask
from flask import request
import BaseUpscale

app = flask.Flask(__name__)
UpscaleModel = BaseUpscale.UpscaleModelPan(device="cpu")

@app.route('/upscale', methods = ['GET', 'POST'])
def upscale():
    payload = request.get_json(force=True)
    try:
        image_id = payload['image_id']
    except:
        return {'error': 'JSON payload invalid'}, 400

    path = UpscaleModel.generate(image_id)["path"]

    return {'path': path}

app.run(
    port = 4486
)